/*
  Warnings:

  - You are about to alter the column `temperature` on the `cm` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.
  - You are about to alter the column `humid` on the `cm` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.
  - You are about to alter the column `gas` on the `cm` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.
  - You are about to alter the column `temperature` on the `sf` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.
  - You are about to alter the column `humid` on the `sf` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.
  - You are about to alter the column `weight` on the `sf` table. The data in that column could be lost. The data in that column will be cast from `Decimal(5,2)` to `Decimal(65,30)`.

*/
-- AlterTable
ALTER TABLE `cm` MODIFY `temperature` DECIMAL(65, 30) NOT NULL,
    MODIFY `humid` DECIMAL(65, 30) NOT NULL,
    MODIFY `gas` DECIMAL(65, 30) NOT NULL;

-- AlterTable
ALTER TABLE `sf` MODIFY `temperature` DECIMAL(65, 30) NOT NULL,
    MODIFY `humid` DECIMAL(65, 30) NOT NULL,
    MODIFY `weight` DECIMAL(65, 30) NOT NULL;
