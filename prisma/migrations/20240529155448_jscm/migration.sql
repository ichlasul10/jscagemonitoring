-- CreateTable
CREATE TABLE `sensordatas` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `humid_1` DECIMAL(5, 2) NOT NULL,
    `humid_2` DECIMAL(5, 2) NOT NULL,
    `humid_3` DECIMAL(5, 2) NOT NULL,
    `temperature_1` DECIMAL(5, 2) NOT NULL,
    `temperature_2` DECIMAL(5, 2) NOT NULL,
    `temperature_3` DECIMAL(5, 2) NOT NULL,
    `gas_1` DECIMAL(5, 2) NOT NULL,
    `gas_2` DECIMAL(5, 2) NOT NULL,
    `gas_3` DECIMAL(5, 2) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
