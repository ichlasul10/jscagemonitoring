/*
  Warnings:

  - You are about to drop the column `gas_2` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `gas_3` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `humid_2` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `humid_3` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `temperature_2` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `temperature_3` on the `sensordata` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `sensordata` DROP COLUMN `gas_2`,
    DROP COLUMN `gas_3`,
    DROP COLUMN `humid_2`,
    DROP COLUMN `humid_3`,
    DROP COLUMN `temperature_2`,
    DROP COLUMN `temperature_3`;
