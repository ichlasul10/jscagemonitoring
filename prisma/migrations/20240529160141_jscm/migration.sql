/*
  Warnings:

  - You are about to drop the column `gas_1` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `humid_1` on the `sensordata` table. All the data in the column will be lost.
  - You are about to drop the column `temperature_1` on the `sensordata` table. All the data in the column will be lost.
  - Added the required column `gas` to the `sensordata` table without a default value. This is not possible if the table is not empty.
  - Added the required column `humid` to the `sensordata` table without a default value. This is not possible if the table is not empty.
  - Added the required column `temperature` to the `sensordata` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `sensordata` DROP COLUMN `gas_1`,
    DROP COLUMN `humid_1`,
    DROP COLUMN `temperature_1`,
    ADD COLUMN `gas` DECIMAL(5, 2) NOT NULL,
    ADD COLUMN `humid` DECIMAL(5, 2) NOT NULL,
    ADD COLUMN `temperature` DECIMAL(5, 2) NOT NULL;
