import express from 'express';
import cmRouter from './cmRoutes.js';
import sfRouter from './sfRoutes.js';

const apiRouter = new express();

apiRouter.use('/cm', cmRouter);
apiRouter.use('/sf', sfRouter);

export default apiRouter;