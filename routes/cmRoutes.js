import cmController from "../controllers/cmController.js";
import express from "express";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

const cmRouter = new express.Router();
/**
 * @swagger
 * components:
 *   schemas:
 *     Cage Monitoring:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           description: The ID of the sensor data.
 *         temperature:
 *           type: number
 *           description: The value of the temperature.
 *         humid:
 *           type: number
 *           description: The value of the humidity in the vicinity.
 *         gas:
 *           type: number
 *           description: The value of gas concentration int the vicinit.
 *         created_at:
 *           type: string
 *           format: date-time
 *           description: The timestamp of the sensor data created.
 *         updated_at:
 *           type: string
 *           format: date-time
 *           description: The timestamp of the sensor data modified.
 */

/**
 * @swagger
 * tags:
 *   name: Sensor Data
 *   description: API endpoints for sensor data
 */
/**
 * @swagger
 * /api/cm/data:
 *   get:
 *     summary: Retrieve sensor data
 *     tags: [Sensor Data]
 *     responses:
 *       200:
 *         description: Successful response
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Cage Monitoring'
 *   post:
 *     summary: Create sensor data
 *     tags: [Sensor Data]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Cage Monitoring'
 *           example:
 *             temperature: 27
 *             humid: 60
 *             gas: 10
 *     responses:
 *       201:
 *         description: Sensor data created successfully
 *       400:
 *         description: Invalid request body
 */



cmRouter.get("/data", cmController.get);
cmRouter.post("/data", cmController.create);

export default cmRouter;