import sfService from "../services/sfService.js";


const create = async (req, res, next) => {
    try {
        const result = await sfService.createSfDataService(req.body);
        return res.status(201).json({

            status: 'success',
            code: 201,
            data: result
        });
    } catch (e) {
        throw e;
    }
    
}


const get = async (req, res, next) => {
    try {
        const result = await sfService.getSfDataService();
        return res.status(201).json({
            status: 'success',
            code: 201,
            data: result
        });
    } catch (e) {
        throw e;
    }
    
}



export default {
    create,
    get
}